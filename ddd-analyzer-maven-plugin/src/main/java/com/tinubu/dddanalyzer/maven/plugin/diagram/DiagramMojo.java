/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.dddanalyzer.maven.plugin.diagram;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.dddanalyzer.maven.plugin.AbstractMojo;
import com.tsquare.dddanalyzer.core.diagram.DddDiagram;
import com.tsquare.dddanalyzer.core.diagram.plantuml.PlantUmlRepresentation;
import com.tsquare.dddanalyzer.core.diagram.plantuml.PlantUmlRepresentation.Creole;
import com.tsquare.dddanalyzer.core.diagram.plantuml.PlantUmlRepresentation.PlantUmlRepresentationConfig;
import com.tsquare.dddanalyzer.core.model.DddModel;

/**
 * Generates UML diagrams from DDD domain.
 */
@Mojo(name = "diagram", defaultPhase = LifecyclePhase.COMPILE,
      requiresDependencyResolution = ResolutionScope.COMPILE)
public class DiagramMojo extends AbstractMojo {

   /** Skip mojo if true. */
   @Parameter(property = "ddd-analyzer.skipDiagram", required = true, defaultValue = "false")
   private boolean skip;

   /**
    * Optional packages to analyze. Packages must be fully qualified.
    */
   @Parameter(property = "ddd-analyzer.packages")
   private List<String> packages;

   /** Diagrams output directory. */
   @Parameter(property = "ddd-analyzer.diagramOutputPath", required = true,
              defaultValue = "${project.build.directory}/generated-sources/site/asciidoc/ddd-analyzer")
   private String diagramOutputPath;

   /** Filter entities. Entities must be fully qualified. */
   @Parameter(property = "ddd-analyzer.diagramFilterEntities")
   private List<String> diagramFilterEntities;

   /** Optional PlantUML theme. */
   @Parameter(property = "ddd-analyzer.diagramTheme")
   private String diagramTheme;

   /** Optional PlantUML header. Can be multiline. */
   @Parameter(property = "ddd-analyzer.diagramHeader")
   private String diagramHeader;

   /** Optional PlantUML header. Can be multiline. */
   @Parameter(property = "ddd-analyzer.diagramLinkLength", required = true, defaultValue = "1")
   private int diagramLinkLength;

   /** Whether to show fields that are references to other domain objects. */
   @Parameter(property = "ddd-analyzer.diagramShowReferenceFields", required = true, defaultValue = "true")
   private boolean diagramShowReferenceFields;

   /**
    * Optional creole notation for class name (DEFAULT, BOLD, ITALIC, MONOSPACED, UNDERLINED,
    * WAVE_UNDERLINED).
    */
   @Parameter(property = "ddd-analyzer.diagramClassNameCreole", required = true, defaultValue = "DEFAULT")
   private Creole diagramClassNameCreole;

   /**
    * Optional creole notation for field type (DEFAULT, BOLD, ITALIC, MONOSPACED, UNDERLINED,
    * WAVE_UNDERLINED).
    */
   @Parameter(property = "ddd-analyzer.diagramFieldTypeCreole", required = true, defaultValue = "DEFAULT")
   private Creole diagramFieldTypeCreole;

   /**
    * Optional creole notation for field name (DEFAULT, BOLD, ITALIC, MONOSPACED, UNDERLINED,
    * WAVE_UNDERLINED).
    */
   @Parameter(property = "ddd-analyzer.diagramFieldNameCreole", required = true, defaultValue = "DEFAULT")
   private Creole diagramFieldNameCreole;

   /**
    * Maximum number of fields to display in a single value/entity. No limit if unset.
    */
   @Parameter(property = "ddd-analyzer.diagramMaxFields")
   private Integer diagramMaxFields;

   @Override
   public void execute() {
      if (!skip) {
         loadModuleClasspath();

         Path outputPath = Paths.get(this.diagramOutputPath);

         getLog().info(String.format("Generate DDD diagrams in '%s'", outputPath));

         DddModel model = new DddModel(packages);

         getLog().debug("DDD model : " + model.domainObjects());

         try {
            Files.createDirectories(outputPath);
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }

         FsDocumentRepository outputRepository =
               new FsDocumentRepository(new FsDocumentConfigBuilder().storagePath(outputPath).build());
         PlantUmlRepresentationConfig plantUmlConfig = new PlantUmlRepresentationConfig()
               .linkLength(diagramLinkLength)
               .showReferenceFields(diagramShowReferenceFields)
               .classNameCreole(diagramClassNameCreole)
               .fieldTypeCreole(diagramFieldTypeCreole)
               .fieldNameCreole(diagramFieldNameCreole);

         if (diagramMaxFields != null) {
            plantUmlConfig = plantUmlConfig.maxFields(diagramMaxFields);
         }
         if (diagramTheme != null) {
            plantUmlConfig = plantUmlConfig.theme(diagramTheme);
         }
         if (diagramHeader != null) {
            plantUmlConfig = plantUmlConfig.header(diagramHeader);
         }

         getLog().debug("PlantUML configuration : " + plantUmlConfig);

         PlantUmlRepresentation representation = new PlantUmlRepresentation(plantUmlConfig);

         new DddDiagram(model).generateAggregateDiagrams(representation,
                                                         e -> diagramFilterEntities.isEmpty()
                                                              || diagramFilterEntities.contains(e),
                                                         outputRepository,
                                                         "aggregate");
      }
   }

}
