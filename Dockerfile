#### Build environment image ####

FROM maven:3.8.4-openjdk-17-slim AS build

ENV MAVEN_OPTS=""
ENV MAVEN_CLI_OPTS="-U -B --show-version -Dstyle.color=always"
ENV BUILD_DIR="/build"

WORKDIR "$BUILD_DIR"

# Caching dependencies
COPY .mvn .mvn
COPY pom.xml .
COPY ddd-analyzer-core/pom.xml ddd-analyzer-core/
COPY ddd-analyzer-maven-plugin/pom.xml ddd-analyzer-maven-plugin/
RUN mvn $MAVEN_CLI_OPTS dependency:go-offline -Dsilent=true

COPY . .

ARG SITE=false
ARG SITE_EXPORT=/export/site.tar.gz
ARG SKIP_TESTS=false
ARG TESTS_EXPORT=/export/tests.tar.gz
ARG CI_JOB_TOKEN
ENV CI_JOB_TOKEN="$CI_JOB_TOKEN"
ARG MAVEN_PHASE=deploy

RUN set -e; \
    cp .docker/buildsdk /bin; \
    eval buildsdk setx mvn $MAVEN_CLI_OPTS -s .mvn/maven-settings.xml clean $MAVEN_PHASE $($SKIP_TESTS && echo "-DskipTests") -DenableJacoco -DinstallAtEnd -DdeployAtEnd; \
    if ! $SKIP_TESTS; then \
    buildsdk report_jacoco_coverage; \
    tests="$(find . -path '*/target/*-reports/TEST-*.xml')" && test ! -z "$tests" && mkdir -p "$(dirname "$TESTS_EXPORT")" && tar czf "$TESTS_EXPORT" $tests; \
    fi; \
    if $SITE; then \
       buildsdk setx mvn $MAVEN_CLI_OPTS site site:stage -DenableJavadocReporting -DskipTests && (cd target/staging && mkdir -p "$(dirname "$SITE_EXPORT")" && tar czf "$SITE_EXPORT" .); \
    fi
