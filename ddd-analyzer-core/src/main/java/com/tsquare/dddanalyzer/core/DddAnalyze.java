/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tsquare.dddanalyzer.core;

import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotates domain object type with information about DDD model.
 */
@Target({ TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Inherited

public @interface DddAnalyze {

   /**
    * Ignore DDD analyzer rules :
    * <ul>
    *    <li>{@code ddd:multiple-aggregate-membership}</li>
    *    <li>{@code ddd:cross-aggregate-reference}</li>
    * </ul>
    *
    * @return ignored rules
    */
   String[] ignoreRules() default { };

   /**
    * Disambiguates id field if multiple fields matches.
    *
    * @return id field name
    */
   String idField() default "";
}
