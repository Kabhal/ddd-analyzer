/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tsquare.dddanalyzer.core.analyzer;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static java.util.stream.Collectors.joining;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import com.tsquare.dddanalyzer.core.model.DddModel;
import com.tsquare.dddanalyzer.core.model.DomainObjectModel;
import com.tsquare.dddanalyzer.core.model.EntityModel;
import com.tsquare.dddanalyzer.core.model.RootEntityModel;

public class DddAnalyzer {

   private final DddModel model;
   private final Consumer<String> warningCallback;
   private final Consumer<String> errorCallback;
   private int warningCount;
   private int errorCount;

   public DddAnalyzer(DddModel model, Consumer<String> warningCallback, Consumer<String> errorCallback) {
      this.model = model;
      this.warningCallback = warningCallback;
      this.errorCallback = errorCallback;

      checkRules();
   }

   public DddAnalyzer(DddModel model) {
      this(model, DddAnalyzer::defaultCallback, DddAnalyzer::defaultCallback);
   }

   public int warningCount() {
      return warningCount;
   }

   public int errorCount() {
      return errorCount;
   }

   private void checkRules() {

      checkExplicitRootEntity();

      Map<RootEntityModel, List<DomainObjectModel>> aggregates = model.normalizeModel().aggregates();

      List<EntityModel> multipleMembershipEntities = list();
      aggregates.forEach((root, aggregate) -> {
         //checkCrossReference(root, aggregate, aggregates);

         checkMultipleAggregateMembership(root, aggregate, aggregates, multipleMembershipEntities);
      });
   }

   /**
    * Checks if an object in the specified aggregate belongs to another aggregate.
    *
    * @param root analyzed aggregate root entity
    * @param aggregate analyzed aggregate domain objects
    * @param aggregates all aggregates
    * @param multipleMembershipEntities mutable list of entities in error to be updated
    */
   private void checkMultipleAggregateMembership(RootEntityModel root,
                                                 List<DomainObjectModel> aggregate,
                                                 Map<RootEntityModel, List<DomainObjectModel>> aggregates,
                                                 List<EntityModel> multipleMembershipEntities) {
      for (DomainObjectModel object : aggregate) {
         if (object instanceof EntityModel && !(multipleMembershipEntities.contains(object))) {
            List<RootEntityModel> objectAggregateMemberRoots = list(root);
            aggregates.forEach((otherRoot, otherAggregate) -> {
               if (!otherRoot.equals(root)) {
                  if (otherAggregate.contains(object)) {
                     objectAggregateMemberRoots.add(otherRoot);
                  }
               }
            });

            if (objectAggregateMemberRoots.size() > 1) {
               if (!object.ignoreRule("multiple-aggregate-membership")) {
                  ruleError(
                        "Multiple aggregate membership : '%s' entity is member of multiple aggregates : %s. An entity must be a member of only one aggregate, it's theoretically possible to have the same entity type referenced in multiple aggregates only if entity \"instances\" are disjoint. Use @DddAnalyze(ignoreRules=\"ddd:multiple-aggregate-membership\") on '%s' class to ignore this rule.",
                        object.type().typeName(),
                        objectAggregateMemberRoots
                              .stream()
                              .map(r -> r.type().typeName())
                              .collect(joining(", ", "[", "]")),
                        object.type().typeName());
                  multipleMembershipEntities.add((EntityModel) object);
               }
            }
         }
      }
   }

   /**
    * Checks if specified aggregate has objects referencing another aggregate
    *
    * @param root analyzed aggregate root entity
    * @param aggregate analyzed aggregate domain objects
    * @param aggregates all aggregates
    */
   private void checkCrossReference(RootEntityModel root,
                                    List<DomainObjectModel> aggregate,
                                    Map<RootEntityModel, List<DomainObjectModel>> aggregates) {
      for (DomainObjectModel object : aggregate) {
         aggregates.forEach((otherRoot, otherAggregate) -> {
            if (!otherRoot.equals(root)) {
               if (otherAggregate.contains(object)) {
                  if (!object.ignoreRule("cross-aggregate-reference")) {
                     ruleError(
                           "Cross-aggregate reference : '%s' aggregate has direct reference to '%s' in '%s' aggregate. Aggregates objects must only reference other aggregates entities using id.  Use @DddAnalyze(ignoreRules=\"ddd:cross-aggregate-reference\") on '%s' class to ignore this rule.",
                           root.type().typeName(),
                           object.type().typeName(),
                           otherRoot.type().typeName(),
                           object.type().typeName());
                  }
               }
            }
         });
      }
   }

   /**
    * Checks if an entity should explicitly implement RootEntity because there's no references to it.
    */
   private void checkExplicitRootEntity() {
      for (DomainObjectModel domainObjectModel : model.domainObjects()) {
         if (domainObjectModel.referencedBy().isEmpty()
             && domainObjectModel instanceof EntityModel
             && !(domainObjectModel instanceof RootEntityModel)) {
            if (!domainObjectModel.ignoreRule("explicit-root-entity")) {
               ruleWarning(
                     "Explicit model : '%s' entity is not referenced by any object, and should probably extends RootEntity.  Use @DddAnalyze(ignoreRules=\"ddd:explicit-root-entity\") on '%s' class to ignore this rule.",
                     domainObjectModel.type().typeName(),
                     domainObjectModel.type().typeName());
            }
         }
      }
   }

   private void ruleWarning(String msg, String... args) {
      warningCount += 1;
      warningCallback.accept(String.format("[Warning] " + msg, args));
   }

   private void ruleError(String msg, String... args) {
      errorCount += 1;
      errorCallback.accept(String.format("[Error] " + msg, args));
   }

   public static void defaultCallback(String message) {
      System.err.println(message);
   }

}
