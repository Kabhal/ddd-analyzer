/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tsquare.dddanalyzer.core.model;

import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import com.tinubu.commons.ddd2.domain.type.DomainObject;

/**
 * Root entity domain object model.
 */
public class RootEntityModel extends EntityModel {

   public RootEntityModel(RawType<? extends DomainObject> type, ValueModel id) {
      super(type, id);
   }

   public RootEntityModel(RawType<?> type,
                          List<FieldModel> fields,
                          ValueModel id,
                          Map<FieldModel, DomainObjectModel> references,
                          List<DomainObjectModel> referencedBy) {
      super(type, fields, id, references, referencedBy);
   }

   public RootEntityModel(RawType<?> type, List<FieldModel> fields, ValueModel id) {
      this(type, fields, id, null, null);
   }

   public static RootEntityModel from(EntityModel entityModel) {
      return new RootEntityModel(entityModel.type,
                                 entityModel.fields,
                                 entityModel.id,
                                 entityModel.references,
                                 entityModel.referencedBy);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", RootEntityModel.class.getSimpleName() + "[", "]")
            .add("id=" + id)
            .add("type=" + type)
            //.add("fields=" + fields)
            .toString();
   }
}
