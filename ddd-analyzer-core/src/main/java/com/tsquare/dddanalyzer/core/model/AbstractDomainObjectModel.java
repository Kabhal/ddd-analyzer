/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tsquare.dddanalyzer.core.model;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static java.util.Collections.emptyList;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;

import com.tinubu.commons.ddd2.domain.type.DomainObject;
import com.tsquare.dddanalyzer.core.DddAnalyze;

public abstract class AbstractDomainObjectModel extends SimpleObjectModel implements DomainObjectModel {

   protected final List<FieldModel> fields;
   protected final Map<FieldModel, DomainObjectModel> references;
   protected final List<DomainObjectModel> referencedBy;

   protected AbstractDomainObjectModel(RawType<? extends DomainObject> type) {
      super(type);
      this.fields = list();
      this.references = map();
      this.referencedBy = list();
   }

   protected AbstractDomainObjectModel(RawType<?> type,
                                       List<FieldModel> fields,
                                       Map<FieldModel, DomainObjectModel> references,
                                       List<DomainObjectModel> referencedBy) {
      super(type);
      this.fields = list(fields);
      this.references = map(references);
      this.referencedBy = list(referencedBy);
   }

   @Override
   @SuppressWarnings("unchecked")
   public RawType<? extends DomainObject> type() {
      return (RawType<? extends DomainObject>) type;
   }

   @Override
   public List<RawType<?>> generics() {
      return emptyList();
   }

   @Override
   public List<FieldModel> fields() {
      return fields;
   }

   @Override
   public Map<FieldModel, DomainObjectModel> references() {
      return references;
   }

   @Override
   public List<DomainObjectModel> referencedBy() {
      return referencedBy;
   }

   @Override
   public boolean ignoreRule(String ruleName) {
      return type().typeClass().map(domainClass -> {
         DddAnalyze dddAnalyze = domainClass.getAnnotation(DddAnalyze.class);
         return dddAnalyze != null && list(dddAnalyze.ignoreRules()).contains(ruleName(ruleName));
      }).orElse(false);
   }

   /**
    * Rule name normalizer.
    *
    * @param ruleName rule name with or without prefix
    *
    * @return rule name with prefix
    */
   private String ruleName(String ruleName) {
      if (ruleName.startsWith("ddd:")) {
         return ruleName;
      } else {
         return "ddd:" + ruleName;
      }
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      if (!super.equals(o)) return false;
      //AbstractDomainObjectModel that = (AbstractDomainObjectModel) o;
      //return Objects.equals(fields, that.fields);
      return true;
   }

   public boolean fullyEquals(Object o) {
      if (!equals(o)) return false;
      AbstractDomainObjectModel that = (AbstractDomainObjectModel) o;
      return Objects.equals(fields, that.fields);
   }

   @Override
   public int hashCode() {
      return Objects.hash(super.hashCode()/*, fields*/);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", AbstractDomainObjectModel.class.getSimpleName() + "[", "]").add("type="
                                                                                                    + type)
                                                                                               //.add("fields=" + fields)
                                                                                               .toString();
   }

}
