/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tsquare.dddanalyzer.core.diagram;

import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tsquare.dddanalyzer.core.model.DddModel;
import com.tsquare.dddanalyzer.core.model.DomainObjectModel;
import com.tsquare.dddanalyzer.core.model.RootEntityModel;

public class DddDiagram {

   private final DddModel model;

   public DddDiagram(DddModel model) {
      this.model = model;
   }

   public void generateSingleDiagram(DddRepresentation representation,
                                     Predicate<String> entityFilter,
                                     DocumentRepository output,
                                     DocumentPath documentId) {
      notNull(representation, "representation");
      notNull(entityFilter, "entityFilter");
      notNull(output, "output");
      notNull(documentId, "documentId");

      List<DomainObjectModel> domainObjects = model.normalizeModel().domainObjects();

      representation.generate(domainObjects,
                              d -> d instanceof RootEntityModel && entityFilter.test(d.type().typeName()),
                              true,
                              output,
                              documentId);
   }

   public void generateAggregateDiagrams(DddRepresentation representation,
                                         Predicate<String> entityFilter,
                                         DocumentRepository output,
                                         String prefix) {
      notNull(representation, "representation");
      notNull(entityFilter, "entityFilter");
      notNull(output, "output");
      notBlank(prefix, "prefix");

      DddModel normalizedModel = model.normalizeModel();
      Map<RootEntityModel, List<DomainObjectModel>> aggregates = normalizedModel.aggregates();

      aggregates.keySet().stream().filter(d -> entityFilter.test(d.type().typeName())).forEach(aggregate -> {
         representation.generate(normalizedModel.domainObjects(),
                                 d -> aggregates.get(aggregate).contains(d),
                                 true,
                                 output,
                                 entityDocumentName(prefix, aggregate));
      });
   }

   public void generateAggregateDiagrams(DddRepresentation representation,
                                         Predicate<String> entityFilter,
                                         DocumentRepository output) {
      generateAggregateDiagrams(representation, entityFilter, output, "aggregate");
   }

   public void generateRootEntityDiagrams(DddRepresentation representation,
                                          Predicate<String> entityFilter,
                                          DocumentRepository output,
                                          String prefix) {
      notNull(representation, "representation");
      notNull(entityFilter, "entityFilter");
      notNull(output, "output");
      notBlank(prefix, "prefix");

      List<DomainObjectModel> domainObjects = model.normalizeModel().domainObjects();

      domainObjects
            .stream()
            .filter(d -> d instanceof RootEntityModel && entityFilter.test(d.type().typeName()))
            .forEach(root -> {
               representation.generate(domainObjects,
                                       d -> root.equals(d),
                                       true,
                                       output,
                                       entityDocumentName(prefix, root));
            });
   }

   public void generateRootEntityDiagrams(DddRepresentation representation,
                                          Predicate<String> entityFilter,
                                          DocumentRepository output) {
      generateRootEntityDiagrams(representation, entityFilter, output, "entity");

   }

   private DocumentPath entityDocumentName(String prefix, DomainObjectModel dom) {
      return DocumentPath.of(prefix + "-" + dom.type().typeName() + ".puml");
   }

}
