/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tsquare.dddanalyzer.core.model;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyList;

import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * Simple object model.
 */
public class SimpleObjectModel implements ObjectModel {

   protected final RawType<?> type;

   public SimpleObjectModel(RawType<?> type) {
      this.type = notNull(type, "type");
   }

   @Override
   public RawType<?> type() {
      return type;
   }

   @Override
   public List<RawType<?>> generics() {
      return emptyList();
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      SimpleObjectModel that = (SimpleObjectModel) o;
      return Objects.equals(type, that.type);
   }

   @Override
   public int hashCode() {
      return Objects.hash(type);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", SimpleObjectModel.class.getSimpleName() + "[", "]").add("type=" + type).toString();
   }

}
